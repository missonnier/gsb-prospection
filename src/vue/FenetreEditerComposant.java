/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vue;

import controle.ControleurEditerComposant;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author eleve
 */
public class FenetreEditerComposant extends javax.swing.JFrame {
    
    private ControleurEditerComposant cfac;
    private DefaultTableModel monTableModele;
    /**
     * Creates new form FenetreEditerComposant
     */
    public FenetreEditerComposant() {        
    }
    
    public void setControleur(ControleurEditerComposant cfac) {
        this.cfac = cfac;
        initComponents();
        this.setVisible(true);
        this.cfac.genererGrille();
    }
    
     public void setUneLigneListeComposant(int code, String libelle){
        //on rajoute la ligne au conteneur de TableModel (qui est un DefaultTableModel) 
        this.monTableModele = (DefaultTableModel) jTable1.getModel();
        this.monTableModele.addRow(new Object[]{String.valueOf(code), libelle});
    }
     
     public String getIdData() {
         int ligne = this.jTable1.getSelectedRow();
         String data;
         data = this.jTable1.getValueAt(ligne, 0).toString();
         return data;
     }
     
     public void supprimerGrille() {
        int longueur = this.monTableModele.getRowCount();
        for (int i = 0; i <longueur; i++) {
            this.monTableModele.removeRow(0);
        }
        this.cfac.genererGrille();
    }
     
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btAjouter = new javax.swing.JButton();
        btSupprimer = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        btModifier = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        btAjouter.setText("Ajouter");
        btAjouter.setActionCommand("AjouterComposant");
        btAjouter.addActionListener(this.cfac);

        btSupprimer.setText("Supprimer");
        btSupprimer.addActionListener(this.cfac);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Numéro", "Libelle"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTable1);

        btModifier.setText("Modifier");
        btModifier.addActionListener(this.cfac);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btAjouter)
                        .addGap(44, 44, 44)
                        .addComponent(btModifier)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btSupprimer))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 371, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 642, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btAjouter)
                    .addComponent(btSupprimer)
                    .addComponent(btModifier))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btAjouter;
    private javax.swing.JButton btModifier;
    private javax.swing.JButton btSupprimer;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables


}
