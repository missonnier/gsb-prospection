package controle;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import modele.ModeleFichiers;
import modele.labo.Labo;
import vue.FenetreVisualiserLabo;

/**
 *
 * @author BESSE Pauline
 * 
 */

public class ControleurVisualiserLabo implements ActionListener{
    private ModeleFichiers modele;
    private FenetreVisualiserLabo fvl;
    
    public ControleurVisualiserLabo(ModeleFichiers modele, FenetreVisualiserLabo fvl) {
         this.modele = modele;
         this.fvl=fvl;
    }
    
    public void setFvl (FenetreVisualiserLabo fvl ) {
        this.fvl=fvl;
    }
    
    public void actionPerformed(ActionEvent e) {
            if (e.getActionCommand().compareTo("OK") == 0 ){
              this.fvl.setVisible(false);
            }
            
    }
 
    public void genererGrille() {
        // on génére la liste de laboratoires
        /*ArrayList<Labo> lesLabos = this.modele.getLesLabos();
        for (Labo a : lesLabos){
            this.fvl.setUneLigneListeLabo(a.getNomL(), a.getChefVenteLabo());
        }*/
    }
   

    
}
