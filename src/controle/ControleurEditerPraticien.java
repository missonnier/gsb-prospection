/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import modele.ModeleFichiers;
import modele.classesmetiers.profsante.ProfessionnelSante;
import vue.FenetreAjouterPraticien;
import vue.FenetreEditerPraticien;
import vue.FenetreModifierPraticien;


/**
 *
 * @author eleve
 */
public class ControleurEditerPraticien implements ActionListener{
    

    private ModeleFichiers modele;
    private FenetreEditerPraticien fep;
    
    public ControleurEditerPraticien(ModeleFichiers modele, FenetreEditerPraticien fep) {
        this.modele = modele;
        this.fep = fep;
    }
    
    public void setFep(FenetreEditerPraticien fep) {
        this.fep = fep;
    }

    
    public void actionPerformed(ActionEvent ae) {
        
        ArrayList<String> dataPraticienSelect = null;
        if (this.fep.getNbSelectedRows() > 0) {
            dataPraticienSelect = this.fep.getDataPraticienSelect();
        }
        
        if (ae.getActionCommand().compareTo("Annuler") == 0){
            this.fep.setVisible(false);
        }
        
        if (ae.getActionCommand().compareTo("ajouterPraticien") == 0){            
            FenetreAjouterPraticien fap = new FenetreAjouterPraticien();
            ControleurAjouterPraticien cap = new ControleurAjouterPraticien(this.modele, fap);
            fap.setCap(cap);
        }
        
        if (ae.getActionCommand().compareTo("supprimerPraticien") == 0 && this.fep.getNbSelectedRows() != 0){            
            //this.modele.supprUnPraticien(dataPraticienSelect);
            this.updateGrille();
        }
        
        if (ae.getActionCommand().compareTo("modifierPraticien") == 0 && this.fep.getNbSelectedRows() != 0) {
            FenetreModifierPraticien fmp = new FenetreModifierPraticien();
            /*ControleurModifierPraticien cmp = new ControleurModifierPraticien(this.modele, fmp);
            fmp.setControleur(cmp, dataPraticienSelect);*/
        }  
    }

    public void genererGrille() {
        // on génére la liste d'aliments
        /*ArrayList<Praticien> lesPraticiens = this.modele.getLesPraticiens();
        for (Praticien p : lesPraticiens){
            this.fep.setUneLigneListePraticien(p.getNumPra(), p.getNomPra(), p.getPrenomPra(), p.getAdrPra(), p.getVillePra(), p.getTypeCodePra());
        }*/
    }
    
    public void updateGrille() {
        // on génére la liste d'aliments
       /* ArrayList<Praticien> lesPraticiens = this.modele.getLesPraticiens();
        if(this.fep.getNbRows()>0){
            this.fep.viderListe();
        }
        for (Praticien p : lesPraticiens) {
            this.fep.setUneLigneListePraticien(p.getNumPra(), p.getNomPra(), p.getPrenomPra(), p.getAdrPra(), p.getVillePra(), p.getTypeCodePra());
        }*/
    }
}

