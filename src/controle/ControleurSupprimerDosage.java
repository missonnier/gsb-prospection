package controle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import modele.ModeleFichiers;
import modele.classesmetiers.medicaments.Dosage;
import vue.FenetreSupprimerDosage;

/**
 *
 * @author PM Lvgn
 */
public class ControleurSupprimerDosage implements ActionListener{
    private ModeleFichiers modele;
    private FenetreSupprimerDosage fsd;
    
    public ControleurSupprimerDosage(ModeleFichiers modele, FenetreSupprimerDosage fsd) {
         this.modele = modele;
         this.fsd = fsd;
    }
    
    public void setFsd (FenetreSupprimerDosage fsd ) {
        this.fsd = fsd;
    }
    public void actionPerformed(ActionEvent ae) {
        if (ae.getActionCommand().compareTo("SupprimerDosage") == 0){
            this.SuppressionDosage(this.fsd.getCode());
            this.fsd.dispose();

        }
    }
    public ArrayList<Integer> getCode() {
       /* ArrayList<Dosage> lesDosages = this.modele.getLesDosages();
        ArrayList<Integer> lesCodes = new ArrayList<Integer>();
        for(Dosage unDosage : lesDosages){
            lesCodes.add(unDosage.getCode());
        }
        return lesCodes;*/
       return null;
    }

    public void SuppressionDosage (int Code) {
        //Dosage d = new Dosage(Code);
        //this.modele.supprDosage(d);
    }
}
