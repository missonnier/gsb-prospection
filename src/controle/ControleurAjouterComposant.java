/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import modele.ModeleFichiers;
import vue.FenetreAjouterComposant;
import vue.FenetreEditerComposant;

/**
 *
 * @author eleve
 */
public class ControleurAjouterComposant implements ActionListener{
    private ModeleFichiers modele;
    private FenetreAjouterComposant fap;
    private ControleurEditerComposant cec;
    private FenetreEditerComposant fac;
    
    public ControleurAjouterComposant(ModeleFichiers modele, FenetreAjouterComposant fap, FenetreEditerComposant fac, ControleurEditerComposant cec) {
         this.modele = modele;
         this.fap = fap;
         this.cec = cec;
         this.fac = fac;
    }    
     
     public void actionPerformed(ActionEvent ae) {
        if (ae.getActionCommand().compareTo("Valider") == 0) {
            //this.modele.setAddComposants(this.fap.getData());
            this.fap.dispose();
            this.fac.supprimerGrille();
        }
    }
}
