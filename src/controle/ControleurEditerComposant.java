/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import modele.ModeleFichiers;
import vue.FenetreAjouterComposant;
import vue.FenetreEditerComposant;
import vue.FenetreModifierComposant;
import vue.FenetreSupprimerComposant;

/**
 *
 * @author eleve
 */
public class ControleurEditerComposant implements ActionListener{  
    
    private ModeleFichiers modele;
    private FenetreEditerComposant fap;
    private FenetreModifierComposant fac;
    
    public ControleurEditerComposant(ModeleFichiers modele, FenetreEditerComposant fap) {
          this.modele = modele;
         this.fap = fap;
    }
    
    public void setFvl (FenetreEditerComposant fap ) {
        this.fap = fap;
    }
    
    public void genererGrille() {
        // on génère la liste de composants
       /* ArrayList<Composant> lesComposants = this.modele.getLesComposants();
        for (Composant a : lesComposants){                
            this.fap.setUneLigneListeComposant(a.getCODE(), a.getLIBELLE());        
        }*/
    }
    
    public void actionPerformed(ActionEvent ae) {
        if (ae.getActionCommand().compareTo("AjouterComposant") == 0) {
            FenetreAjouterComposant fmv = new FenetreAjouterComposant();
            ControleurAjouterComposant cmv = new ControleurAjouterComposant(this.modele, fmv, this.fap, this);
            fmv.setControleur(cmv);
        }
        else if(ae.getActionCommand().compareTo("Supprimer") == 0) {    
            String getIdData = this.fap.getIdData();
            FenetreSupprimerComposant fmv = new FenetreSupprimerComposant();
            ControleurSupprimerComposant cmv = new ControleurSupprimerComposant(this.modele, fmv, this.fap, this);
            fmv.setControleur(cmv);
        }     
        else if(ae.getActionCommand().compareTo("Modifier") == 0) {    
            FenetreModifierComposant fmv = new FenetreModifierComposant();
            ControleurModifierComposant cmv = new ControleurModifierComposant(this.modele, fmv, this.fap, this);
            fmv.setControleur(cmv);
        }  
    }
}
