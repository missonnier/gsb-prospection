package controle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import modele.ModeleFichiers;
import modele.classesmetiers.medicaments.Dosage;
import vue.FenetreVisualiserDosage;


/**
 *
 * @author PM Lvgn
 */
public class ControleurVisualiserDosage implements ActionListener{
    private ModeleFichiers modele;
    private FenetreVisualiserDosage fvd;
    
    public ControleurVisualiserDosage(ModeleFichiers modele, FenetreVisualiserDosage fvd) {
         this.modele = modele;
         this.fvd = fvd;
    }
    
    public void setFvd (FenetreVisualiserDosage fvd ) {
        this.fvd = fvd;
    }
    public void actionPerformed(ActionEvent ae) {
        if (ae.getActionCommand().compareTo("Annuler") == 0 || ae.getActionCommand().compareTo("OK") == 0){
            this.fvd.setVisible(false);
        }
    }
     public void genererGrille() {
        /*ArrayList<Dosage> lesDosages = this.modele.getLesDosages();
        System.out.println(lesDosages.size());
        for (Dosage d : lesDosages){
            this.fvd.setUneLigneListeDosage(d.getCode(), d.getQuantite(), d.getUnite());
        }*/
    }
}
