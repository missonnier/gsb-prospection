package controle;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import modele.ModeleFichiers;
import modele.labo.Labo;
import vue.FenetreAjouterLabo;

/**
 *
 * @author BESSE Pauline
 * 
 */

public class ControleurAjouterLabo implements ActionListener{
    private ModeleFichiers modele;
    private FenetreAjouterLabo fal;
    
    public ControleurAjouterLabo(ModeleFichiers modele, FenetreAjouterLabo fal) {
         this.modele = modele;
         this.fal = fal;
    }
    
    public void setFvl (FenetreAjouterLabo fal ) {
        this.fal = fal;
    }
    
    public void actionPerformed(ActionEvent e) {
             if (e.getActionCommand().compareTo("Annuler") == 0 ){
            this.fal.setVisible(false);
        }
             if (e.getActionCommand().compareTo("Ajouter") == 0 ){
            //on récupère les chaînes de caractères correspondant à une personne
            //dans la vue
            ArrayList<String> laboAAjouter = this.fal.getLaboAjoute();
            
            //on ajoute la personne dans le modèle 
            //laboAAjouter.get(1) contient le nom 
            //laboAAjouter.get(2) contient le code 
            //laboAAjouter.get(0) contient la chef des ventes
            Labo lab = new Labo (laboAAjouter.get(1), laboAAjouter.get(0), laboAAjouter.get(2) );
            //this.modele.setLabo(lab);
            
            //on ferme la fenêtre
            this.fal.setVisible(false);
        }
    }
    
    
   
   

    
}
