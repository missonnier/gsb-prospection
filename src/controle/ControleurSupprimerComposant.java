/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package controle;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import modele.ModeleFichiers;
import vue.FenetreEditerComposant;
import vue.FenetreSupprimerComposant;

/**
 *
 * @author eleve
 */
public class ControleurSupprimerComposant implements ActionListener{
    
    private ModeleFichiers modele;
    private FenetreSupprimerComposant fap;
    private FenetreEditerComposant fac;
    private ControleurEditerComposant cec;
    
    public ControleurSupprimerComposant(ModeleFichiers modele, FenetreSupprimerComposant fap, FenetreEditerComposant fac, ControleurEditerComposant cec) {
         this.modele = modele;
         this.fap = fap;
         this.fac = fac;
         this.cec = cec;
    }
    
    public void setFvl (FenetreSupprimerComposant fap ) {
        this.fap = fap;
    }

    public void actionPerformed(ActionEvent ae) {
        if (ae.getActionCommand().compareTo("OUI") == 0) {            
            //this.modele.setSupprComposants(this.fac.getIdData()); 
            this.fap.dispose();
            this.fac.supprimerGrille();         
        }
        if (ae.getActionCommand().compareTo("NON") == 0) {
            this.fap.dispose();
        }        
    }
    
}
