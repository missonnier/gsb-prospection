/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import modele.ModeleFichiers;
import vue.FenetreEditerComposant;
import vue.FenetreModifierComposant;

/**
 *
 * @author eleve
 */
public class ControleurModifierComposant implements ActionListener{
    
    private ModeleFichiers modele;
    private FenetreModifierComposant fap;
    private FenetreEditerComposant fec;
    private ControleurEditerComposant cec;
    
    public ControleurModifierComposant(ModeleFichiers modele, FenetreModifierComposant fap, FenetreEditerComposant fec, ControleurEditerComposant cec) {
          this.modele = modele;
          this.fap = fap;
         this.fec = fec;
         this.cec = cec;
    }    

   
    public void actionPerformed(ActionEvent ae) {
        if (ae.getActionCommand().compareTo("Modifier") == 0) {            
                    //this.modele.modifierComposant(this.fap.getLibelle() ,this.fec.getIdData()); 
                    this.fap.dispose();
                    this.fec.supprimerGrille();         
                }
    }

    public String getNumeroSelectionne() {
        return this.fec.getIdData();
    }
    
    public String getLibelle(){
        /*return modele.getLesComposantsBis(this.fec.getIdData());
         */
        return null;
    }
    
}
