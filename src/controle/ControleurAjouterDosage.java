package controle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import modele.ModeleFichiers;
import modele.classesmetiers.medicaments.Dosage;
import vue.FenetreAjouterDosage;

/**
 *
 * @author PM Lvgn
 */
public class ControleurAjouterDosage implements ActionListener{
    private ModeleFichiers modele;
    private FenetreAjouterDosage fad;
    
    public ControleurAjouterDosage(ModeleFichiers modele, FenetreAjouterDosage fad) {
         this.modele = modele;
         this.fad = fad;
    }
    
    public void setFad (FenetreAjouterDosage fad ) {
        this.fad = fad;
    }
    public void actionPerformed(ActionEvent ae) {
        if (ae.getActionCommand().compareTo("AjouterDosage") == 0){
            
            ArrayList<String> dosageAAjouter = this.fad.getDosageAjoute();
            
            //Dosage p = new Dosage (Integer.parseInt(dosageAAjouter.get(0)), Integer.parseInt(dosageAAjouter.get(1)), dosageAAjouter.get(2) );
            //this.modele.setDosage(p);
            
            this.fad.dispose();
        }
        if (ae.getActionCommand().compareTo("AnnulerDosage") == 0){
            this.fad.dispose();
        }
    }
}
