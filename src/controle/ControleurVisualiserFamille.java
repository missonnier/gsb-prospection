package controle;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import modele.ModeleFichiers;
import vue.FenetreVisualiserFamille;

/**
 *
 * @author Benoît FRESNEL & Benoît SERRE
 * 
 */

public class ControleurVisualiserFamille implements ActionListener{
    private ModeleFichiers modele;
    private FenetreVisualiserFamille fvf;
    
    public ControleurVisualiserFamille(ModeleFichiers modele, FenetreVisualiserFamille fvf) {
         this.modele = modele;
         this.fvf = fvf;
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().compareTo("Quitter") == 0 ){
          this.fvf.dispose();
        }
        else if (e.getActionCommand().compareTo("OK") == 0 ){
          this.genererGrille();
        }
    }
 
    public void genererGrille() {
        // on génére la liste de familles de médicaments
       /* ArrayList<Famille> lesFamilles = this.modele.getLesFamilles();
 
        for (Famille a : lesFamilles){
            this.fvf.setUneLigneListeFamille(a.getCode(), a.getLibelle());
        }*/
       
    }
   

    
}