/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import modele.ModeleFichiers;
import modele.classesmetiers.profsante.Region;
import vue.FenetreMenu;
import vue.FenetreSelectionRegion;

/**
 *
 * @author eleve
 */
public class ControleurSelectionRegion implements ActionListener{
    private ModeleFichiers modele;
    private FenetreSelectionRegion fsr;
    private ControleurEditerComposant cec;

    
    public ControleurSelectionRegion(FenetreSelectionRegion fsr, ModeleFichiers modele) {
         this.modele = modele;
         this.fsr = fsr;
    }    
     
     public void actionPerformed(ActionEvent ae) {
        if (ae.getActionCommand().compareTo("Valider") == 0) {
            //récupere la region selectionnée
            String s = this.fsr.getSelectedItem();
            //on charge le modèle en fonction de la région passée en paramètres
            //this.modele.setRegion(s);
            FenetreMenu maFenetre = new FenetreMenu();
            // le controleur doit connaitre le modèle
            ControleurMenu monControleur = new ControleurMenu(this.modele, maFenetre);
            //la vue connait le controleur
            //cette méthode initialise les composants Swing et affiche la fenetre
            maFenetre.setCfm(monControleur);
            this.fsr.dispose();
        }
    }

    public ArrayList<Region> getRegions() {
        return this.modele.getLesRegions();
    }
}
