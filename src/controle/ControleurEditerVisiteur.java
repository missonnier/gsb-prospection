package controle;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import modele.ModeleFichiers;
import modele.labo.Visiteur;
import vue.FenetreEditerVisiteur;
import modele.labo.Labo;
import vue.FenetreModifierVisiteur;

/**
 *
 * @author bancarel valentin
 */
public class ControleurEditerVisiteur implements ActionListener {

    private ModeleFichiers modele;
    private FenetreEditerVisiteur fev;

    public ControleurEditerVisiteur(ModeleFichiers modele, FenetreEditerVisiteur fev) {
        this.modele = modele;
        this.fev = fev;
    }

    public void setFea(FenetreEditerVisiteur fea) {
        this.fev = fea;
    }

    public void actionPerformed(ActionEvent ae) {
        ArrayList<String> dataVisiteurAAjouter = this.fev.getDataVisiteurAAjouter();
        ArrayList<String> dataVisiteurSelect = null;
        if (this.fev.getNbSelectedRows() > 0) {
            dataVisiteurSelect = this.fev.getDataVisiteurSelect();
        }

        if (ae.getActionCommand().compareTo("ModifierVisiteur") == 0 && this.fev.getNbSelectedRows() != 0) {
            FenetreModifierVisiteur fmv = new FenetreModifierVisiteur();
            ControleurModifierVisiteur cmv = new ControleurModifierVisiteur(this.modele, fmv);
            fmv.setControleur(cmv, dataVisiteurSelect);
        } else if (ae.getActionCommand().compareTo("Actualiser") == 0) {
            this.updateGrille();
        } else if (ae.getActionCommand().compareTo("SupprimerVisiteur") == 0 && this.fev.getNbSelectedRows() != 0) {
            //this.modele.supprUnVisiteurs(dataVisiteurSelect);
            this.updateGrille();
        } else if (ae.getActionCommand().compareTo("AjouterVisiteur") == 0) {
            //this.modele.setUnVisiteurs(dataVisiteurAAjouter);
            this.updateGrille();
        } else if (ae.getActionCommand().compareTo("Quitter") == 0) {
            this.fev.dispose();
        }

    }

    public void setListBoxLabo() {
        /*ArrayList<Labo> lesLabos = this.modele.getLesLabos();
        for (Labo unLabo : lesLabos) {
            this.fev.addItemListLabo(unLabo.getCodeL());
        }*/
    }

    public void updateGrille() {
        // on génére la liste d'aliments
        /*ArrayList<Visiteur> lesVisiteurs = this.modele.getLesVisiteurs();
        if (this.fev.getNbRows() > 0) {
            this.fev.viderListe();
        }
        for (Visiteur v : lesVisiteurs) {
            this.fev.setUneLigneListeVisiteur(v.getMatricule(), v.getNom(), v.getPrenom(), v.getAdresse(), v.getCodePostal(), v.getVille(), v.getDateEmbauche(), v.getCodeSecteur(), v.getCodeLaboratoire());
        }
        this.fev.selectAuto();*/
    }
}
