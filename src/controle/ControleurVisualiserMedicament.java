/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import modele.ModeleFichiers;
import modele.classesmetiers.medicaments.Medicament;
import vue.FenetreVisualiserMedicament;
//import modele.classesmetiers.Personne;
/**
 *
 * @author eleve
 */
public class ControleurVisualiserMedicament implements ActionListener {
    private FenetreVisualiserMedicament fvm;
    private ModeleFichiers modele;
    
    @Override
    public void actionPerformed(ActionEvent e) {
       if (e.getActionCommand().compareTo("OK") == 0)
                 
            //on ferme la fenêtre
            this.fvm.setVisible(true);
    }
    
     public ControleurVisualiserMedicament(ModeleFichiers modele, FenetreVisualiserMedicament fan){
        this.modele = modele;
        this.fvm = fan;
    }
 
    public void setFan(FenetreVisualiserMedicament fan){
        this.fvm = fan;
    }
    
     public ArrayList<Medicament> getMedicament() {
        //va chercher la liste dans le modèle
        ArrayList<Medicament> lesMedics = this.modele.getLesMedicaments();
        return lesMedics;
    }
}
