package controle;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import modele.ModeleFichiers;
import vue.FenetreAjouterLabo;

import vue.FenetreAjouterMedicament;

import vue.FenetreEditerVisiteur;
import vue.FenetreMenu;
import vue.FenetreVisualiserLabo;
import vue.FenetreEditerComposant;
//import vue.FenetreEditerPraticien;
import vue.FenetreMajMedicament;
import vue.FenetreSupprimerLabo;
import vue.FenetreSupprimerMedicament;
import vue.FenetreVisualiserMedicament;
import vue.FenetreVisualiserDosage;
import vue.FenetreAjouterDosage;
import vue.FenetreEditerPraticien;
import vue.FenetreModifierDosage;
import vue.FenetreModifierLabo;
import vue.FenetreSupprimerDosage;
import vue.FenetreVisualiserFamille;
        

/**
 *
 * @author Fabrice Missonnier
 * 
 */

public class ControleurMenu implements ActionListener{
    private ModeleFichiers modele;
    private FenetreMenu vue;
    
    public ControleurMenu(ModeleFichiers modele, FenetreMenu vue) {
         this.modele = modele;
         this.vue = vue;
    }
            
    public void actionPerformed(ActionEvent e) {
        
        if (e.getActionCommand().compareTo("editerPraticien") == 0){
            FenetreEditerPraticien fep = new FenetreEditerPraticien ();
            ControleurEditerPraticien cep = new ControleurEditerPraticien(this.modele, fep);
            fep.setControleur(cep);
        }
        
        if (e.getActionCommand().compareTo("EditerVisiteur") == 0){
            FenetreEditerVisiteur fev = new FenetreEditerVisiteur ();
            ControleurEditerVisiteur cev = new ControleurEditerVisiteur(this.modele, fev);
            fev.setControleur(cev);
        }
        else if (e.getActionCommand().compareTo("visualiserLabo") == 0){
            //on ouvre la fenetre de visualisation des labos
            FenetreVisualiserLabo fvl = new FenetreVisualiserLabo ();
            ControleurVisualiserLabo cvl = new ControleurVisualiserLabo(this.modele, fvl);
            fvl.setCvl(cvl);
        }
        else if (e.getActionCommand().compareTo("ajouterLabo") == 0){
            //on ouvre la fenetre d'ajout des labos
            FenetreAjouterLabo fal = new FenetreAjouterLabo ();
            ControleurAjouterLabo cal = new ControleurAjouterLabo(this.modele, fal);
            fal.setCvl(cal);
        }
        else if (e.getActionCommand().compareTo("modifierLabo") == 0){
            //on ouvre la fenetre de suppression des labos
            FenetreModifierLabo fml = new FenetreModifierLabo ();
            ControleurModifierLabo cml = new ControleurModifierLabo(this.modele, fml);
            fml.setControleur(cml);
            
        }
        else if (e.getActionCommand().compareTo("supprimerLabo") == 0){
            //on ouvre la fenetre de suppression des labos
            FenetreSupprimerLabo fsl = new FenetreSupprimerLabo ();
            ControleurSupprimerLabo csl = new ControleurSupprimerLabo(this.modele, fsl);
            fsl.setControleur(csl);
            
        } else if (e.getActionCommand().compareTo("EditerComposant") == 0){
            FenetreEditerComposant fap = new FenetreEditerComposant();
            ControleurEditerComposant cap = new ControleurEditerComposant(this.modele, fap);
            fap.setControleur(cap);
        }
           
        if (e.getActionCommand().compareTo("AjouterMedicament") == 0){
            //on ouvre la fenetre d'ajout d'un medicament
            FenetreAjouterMedicament fam = new FenetreAjouterMedicament();
            ControleurAjouterMedicament cam = new ControleurAjouterMedicament(this.modele, fam);
            fam.setControleur(cam);
        }
        if(e.getActionCommand().compareTo("supprimerMedicament") ==0){
             FenetreSupprimerMedicament fsm = new FenetreSupprimerMedicament ();
            ControleurSupprimerMedicament csm = new ControleurSupprimerMedicament(this.modele, fsm);
             fsm.setControleur(csm); 
        }
              
        if(e.getActionCommand().compareTo("visualiserMedicament") ==0){
            FenetreVisualiserMedicament fvm = new FenetreVisualiserMedicament ();
            ControleurVisualiserMedicament cvm = new ControleurVisualiserMedicament(this.modele, fvm);
            fvm.setControleur(cvm); 
        }

        if(e.getActionCommand().compareTo("MAJMedicament") == 0){
            FenetreMajMedicament fmm = new FenetreMajMedicament ();
            ControleurMajMedicament cmm = new ControleurMajMedicament(this.modele, fmm);
            fmm.setControleur(cmm); 
        }
        
        // -------------------------------- Debut Dosage ---------------------------------------------
        if (e.getActionCommand().compareTo("AjouterDosage") == 0){
            FenetreAjouterDosage fad = new FenetreAjouterDosage ();
            ControleurAjouterDosage cad = new ControleurAjouterDosage (this.modele, fad);
            fad.setCad(cad);
        }
        if(e.getActionCommand().compareTo("SupprimerDosage") ==0){
             FenetreSupprimerDosage  fsd = new FenetreSupprimerDosage  ();
            ControleurSupprimerDosage  csd = new ControleurSupprimerDosage (this.modele, fsd);
             fsd.setCsd(csd);
        }
              
        if(e.getActionCommand().compareTo("VisualiserDosage") ==0){
            FenetreVisualiserDosage  fvd = new FenetreVisualiserDosage  ();
            ControleurVisualiserDosage  cvd = new ControleurVisualiserDosage (this.modele, fvd);
            fvd.setCvd(cvd);
        }

        if(e.getActionCommand().compareTo("ModifierDosage") == 0){
            FenetreModifierDosage fmd = new FenetreModifierDosage  ();
            ControleurModifierDosage  cmd = new ControleurModifierDosage (this.modele, fmd);
            fmd.setCmd(cmd);
        }
        // -------------------------------- Fin Dosage -----------------------------------------------
        
        if(e.getActionCommand().compareTo("visualiserFamille") == 0){
            FenetreVisualiserFamille fvf = new FenetreVisualiserFamille ();
            ControleurVisualiserFamille cvf = new ControleurVisualiserFamille(this.modele, fvf);
            fvf.setCvf(cvf);
        }

    }

    public void setMaFenetre(FenetreMenu vue) {
        this.vue = vue;
    }

    
}
