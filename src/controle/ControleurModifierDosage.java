package controle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import modele.ModeleFichiers;
import modele.classesmetiers.medicaments.Dosage;
import vue.FenetreModifierDosage;

/**
 *
 * @author PM Lvgn
 */
public class ControleurModifierDosage implements ActionListener{
    private ModeleFichiers modele;
    private FenetreModifierDosage fmd;
    
    public ControleurModifierDosage(ModeleFichiers modele, FenetreModifierDosage fmd) {
         this.modele = modele;
         this.fmd = fmd;
    }
    
    public void setFmd (FenetreModifierDosage fmd ) {
        this.fmd = fmd;
    }
    public void actionPerformed(ActionEvent ae) {
        if (ae.getActionCommand().compareTo("ModifierDosage") == 0){
            
            ArrayList<String> dosageAModifier = this.fmd.getDosageModifier();
            
           // Dosage p = new Dosage (Integer.parseInt(dosageAModifier.get(0)), Integer.parseInt(dosageAModifier.get(1)), dosageAModifier.get(2) );
            //this.modele.upDosage(p);
            
            this.fmd.dispose();
        }
        if (ae.getActionCommand().compareTo("AnnulerDosage") == 0){
            this.fmd.dispose();
        }
    }
}
