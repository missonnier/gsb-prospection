/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controle;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import modele.ModeleFichiers;
import modele.labo.Labo;
import modele.classesmetiers.medicaments.Medicament;
import vue.FenetreSupprimerLabo;

/**
 *
 * @author BESSE Pauline
 */
public class ControleurSupprimerLabo implements ActionListener{
    private ModeleFichiers modele;
    private FenetreSupprimerLabo fsl;
    
    public ControleurSupprimerLabo(ModeleFichiers modele, FenetreSupprimerLabo fsl) {
         this.modele = modele;
         this.fsl = fsl;
    }
    
    public void setFsl (FenetreSupprimerLabo fsl ) {
        this.fsl = fsl;
    }
    
    public void actionPerformed(ActionEvent e) {
        
        if (e.getActionCommand().compareTo("Supprimer") == 0){
           this.SuppressionLabo(this.fsl.getNomDuLabo());
           this.fsl.setVisible(false);
        }    
    }
    
     public ArrayList<String> getNoms() {
        //va chercher la liste dans le modèle
        /*ArrayList<Labo> lesLabos = this.modele.getLesLabos();
        ArrayList<String> lesNomsDesLabos = new ArrayList<String>();
        
        //on concatène le Nom du médicament et le dépôt légal
        for(Labo unLabo : lesLabos){
            lesNomsDesLabos.add(unLabo.getNomL());
        }
        return lesNomsDesLabos;*/
        return null;
    }
     
    public void SuppressionLabo(String nom)
    {
        Labo lab = new Labo(nom);
        
       // this.modele.deleteLabo(lab);
    }

}
