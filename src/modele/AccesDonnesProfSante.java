/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;


/*import chargecsv.CategPro;
import chargecsv.Commune;
import chargecsv.Profession;
import chargecsv.ProfessionnelSante;
import chargecsv.Site;
import chargecsv.Structure;*/
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import modele.labo.CategPro;
import modele.classesmetiers.profsante.Commune;
import modele.classesmetiers.profsante.Departement;
import modele.classesmetiers.profsante.Profession;
import modele.classesmetiers.profsante.ProfessionnelSante;
import modele.classesmetiers.profsante.Region;
import modele.classesmetiers.profsante.Site;
import modele.classesmetiers.profsante.Structure;

/**
 * Created by adgsb on 05/02/2017.
 */

public class AccesDonnesProfSante{
    private HashMap<String, Profession> lesProfessions;
    private HashMap<String, CategPro> lesCategPros;
    private HashMap<String, Site> lesSites;
    private HashMap<String, Structure> lesStructures;
    private HashMap<String, ProfessionnelSante> lesProfessionelsDeSante;
    private HashMap<String, Region> lesRegions;
    private HashMap<String, Departement> lesDepartements;
    private HashMap<String, Commune> lesCommunes;
    
    public AccesDonnesProfSante() {
        //this.chargeToutDepuisCSV();
        
        this.creerHashMapProfessionnelSanteEnFonctionRegion();
        this.chargeRegionFIC();
        
       
    }
    
    private void chargeToutDepuisCSV(){
        //on a les trois HashMap Region, Departement et Commune liés
        this.chargeRegionFromCSV();
        this.chargeDepartementFromCSV();
        this.chargeCommuneFromCSV();
        
        //Si on ne sérialise que commune, il fait les trois autres en profondeur 
        //dans le même fichier
        //this.serialiseCommune();
        
        //pour les professionnels de santé, on a besoin de charger profession, categorie pro
        //et structure
        this.chargeProfessionEtCategProfFromCSV();
        //pour structure on a besoin de commune, donc c'est ok
        this.chargeStructureFromCSV();
        this.chargeProfSanteFromCSV();
        this.serialiseProfSante();
    }
    
    public void setRegion(String laRegion){
        BufferedReader br = null;
        String line;
      
        try {
            String nomFichier;
            if (laRegion.compareTo("") == 0){
                nomFichier = "profsante.ser";
            }
            else{
                nomFichier = laRegion + "_profsante.ser";
            }
            String fichierSrc =  System.getProperty("user.dir") + File.separator + "fichiersDest" + File.separator + nomFichier;
            FileInputStream fichier = new FileInputStream(fichierSrc);
            ObjectInputStream ois =  new ObjectInputStream(fichier) ;

            //deserialisation du HashMap des professionnels de santé (le HashMap est sérialisé en profondeur et contient les structures, professions, départements et les 
            //régions
            this.lesProfessionelsDeSante = (HashMap<String,ProfessionnelSante>)ois.readObject();
            
        } catch (FileNotFoundException e) {
            Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException | ClassNotFoundException e) {
            Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }
        
        //charge le HashMap de structure en fonction du HashMap de ProfSante
        //charge le HashMap de profession en fonction du HashMap de ProfSante
        this.lesStructures = new HashMap();
        this.lesProfessions = new HashMap();
        this.lesCategPros = new HashMap();
        
        this.lesProfessionelsDeSante.forEach((k,v)->{
            //on teste si le département existe déjà dans le hashmap
            if (v.getLaStructure() != null){
                Structure d = this.lesStructures.get(v.getLaStructure().getIdentifiantStruct());
                if (d == null) {
                    this.lesStructures.put(v.getLaStructure().getIdentifiantStruct(), v.getLaStructure());        
                } 
            }
            
            if (v.getLaProfession() != null){
                Profession p = this.lesProfessions.get(v.getLaProfession().getCode());
                if (p == null) {
                    this.lesProfessions.put(v.getLaProfession().getCode(), v.getLaProfession());        
                } 
            }
            
            if (v.getLaCategorieProf() != null){
                CategPro cp = this.lesCategPros.get(v.getLaCategorieProf().getCode());
                if (cp == null) {
                    this.lesCategPros.put(v.getLaCategorieProf().getCode(), v.getLaCategorieProf());        
                } 
            }
        });

        //charge le HashMap de commune en fonction du HashMap de structure
        this.lesCommunes = new HashMap();
        
        this.lesStructures.forEach((k,v)->{
            //on teste si le département existe déjà dans le hashmap
            if (v.getLaCommune()!= null){
                Commune c = this.lesCommunes.get(v.getLaCommune().getCode());
                if (c == null) {
                    this.lesCommunes.put(v.getLaCommune().getCode(), v.getLaCommune());        
                } 
            }
        });

        //charge le HashMap de département en fonction du HashMap de commune 
        this.lesDepartements = new HashMap();
        
        this.lesCommunes.forEach((k,v)->{
            //on teste si le département existe déjà dans le hashmap
            Departement d = this.lesDepartements.get(v.getLeDepartement().getCode());
            if (d == null) {
                this.lesDepartements.put(v.getLeDepartement().getCode(), v.getLeDepartement());        
            } 
        });

        //charge le HashMap de régions en fonction du HashMap de département
        this.lesRegions = new HashMap();
        this.lesDepartements.forEach((k,v)->{
            //on teste si le département existe déjà dans le hashmap
            Region r = this.lesRegions.get(v.getLaRegion().getCode());
            if (r == null) {
                this.lesRegions.put(v.getLaRegion().getCode(), v.getLaRegion());        
            } 
        });
        
        
    }
     
            
    private void creerHashMapProfessionnelSanteEnFonctionRegion(){
        this.lesRegions.forEach((k,v)->{
            HashMap<String, ProfessionnelSante> tmp = new HashMap();
            
            this.lesProfessionelsDeSante.forEach((k2,v2)->{
                try{
                    if (v2.getLaStructure().getLaCommune().getLeDepartement().getLaRegion() == v){
                        tmp.put(k2, v2);
                    }
                }
                catch (NullPointerException  ex){
                }
                
            });
            BufferedReader br = null;
            
            try {
                String nomFic = v.getNom() + "_profsante.ser";
                String fichierDest =  System.getProperty("user.dir") + File.separator + "fichiersDest" + File.separator + nomFic;
                ObjectOutputStream oos =  new ObjectOutputStream(new FileOutputStream(fichierDest)) ;
                oos.writeObject(tmp) ;
            } catch (FileNotFoundException e) {
                Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
            } catch (IOException e) {
                Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
            } finally {
                if (br != null) {
                    try {
                        br.close();
                    } catch (IOException e) {
                        Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
                    }
                }
            }
            
        });
    }
    
    
    /***
     * REGIONS
     */
    
    private void chargeRegionFromCSV() {
        BufferedReader br = null;
        String line;
        String cvsSplitBy = ";";

        try {
            String fichierSrc =  System.getProperty("user.dir") + File.separator + "fichiersSrc" + File.separator + "CommunesDépartementsRegions"+ File.separator + "reg2016-utf8.csv";
           
            br = new BufferedReader(new FileReader(fichierSrc));
            line = br.readLine();
            
            this.lesRegions = new HashMap();
            
            while ((line = br.readLine()) != null) {
                String[] region = line.split(cvsSplitBy);
                Region r = new Region (region[0], region[4]);
                this.lesRegions.put(region[0], r);
                
            }
        } catch (FileNotFoundException e) {
            Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException e) {
            Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }
    }
    
    private void serialiseRegion() {
        BufferedReader br = null;
        String line;
        String cvsSplitBy = ";";

        try {
            String fichierDest =  System.getProperty("user.dir") + File.separator + "fichiersDest" + File.separator + "regions.ser";
            ObjectOutputStream oos =  new ObjectOutputStream(new FileOutputStream(fichierDest)) ;
            oos.writeObject(this.lesRegions) ;
        } catch (FileNotFoundException e) {
            Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException e) {
            Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }
    }

    private void chargeRegionFIC() {
        BufferedReader br = null;
        String line;
      
        try {
            String fichierSrc =  System.getProperty("user.dir") + File.separator + "fichiersDest" + File.separator + "regions.ser";
            FileInputStream fichier = new FileInputStream(fichierSrc);
            ObjectInputStream ois =  new ObjectInputStream(fichier) ;

            //deserialisation du HashMap
            this.lesRegions = (HashMap<String,Region>)ois.readObject();           
        } catch (FileNotFoundException e) {
            Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException | ClassNotFoundException e) {
            Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }
    }
    
    
    /*********************
     * 
     * DEPARTEMENTS
     * 
     */
    
    private void chargeDepartementFromCSV() {
        BufferedReader br = null;
        String line;
        String cvsSplitBy = ";";

        try {
            String fichierSrc =  System.getProperty("user.dir") + File.separator + "fichiersSrc" + File.separator + "CommunesDépartementsRegions"+ File.separator + "depts2016-utf8.csv";
            br = new BufferedReader(new InputStreamReader(new FileInputStream(fichierSrc), "ISO-8859-1"));
           
            line = br.readLine();
            
            this.lesDepartements = new HashMap();
           
            while ((line = br.readLine()) != null) {
                String[] dep = line.split(cvsSplitBy);
                //s'il y a un 0 devant le code du département on l'enlève
                String depName = dep[1];
                /*if (dep[1].substring(0, 1).compareTo("0") == 0){
                    depName = dep[1].substring(1, dep[1].length());
                }*/
                Region r = this.lesRegions.get(dep[0]);
                Departement d = new Departement(depName, dep[5], r);
                this.lesDepartements.put(depName, d);
            }
            
        } catch (FileNotFoundException e) {
            Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException e) {
            Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }
    }    
    
    /*private void serialiseDepartement() {
        BufferedReader br = null;
        String line;
        String cvsSplitBy = ";";

        try {
            String fichierDest =  System.getProperty("user.dir") + File.separator + "fichiersDest" + File.separator + "departements.ser";
            ObjectOutputStream oos =  new ObjectOutputStream(new FileOutputStream(fichierDest)) ;
            oos.writeObject(this.lesDepartements) ;
        } catch (FileNotFoundException e) {
            Logger.getLogger(ModeleFichiers.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException e) {
            Logger.getLogger(ModeleFichiers.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    Logger.getLogger(ModeleFichiers.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }
    }
     
    private void chargeDepartementFIC() {
        BufferedReader br = null;
        String line;
      
        try {
            String fichierSrc =  System.getProperty("user.dir") + File.separator + "fichiersDest" + File.separator + "departements.ser";
            FileInputStream fichier = new FileInputStream(fichierSrc);
            ObjectInputStream ois =  new ObjectInputStream(fichier) ;

            //deserialisation du HashMap
            this.lesDepartements = (HashMap<String,Departement>)ois.readObject();

        } catch (FileNotFoundException e) {
            Logger.getLogger(ModeleFichiers.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException | ClassNotFoundException e) {
            Logger.getLogger(ModeleFichiers.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    Logger.getLogger(ModeleFichiers.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }
    }*/
    
     
     /******
      * 
      *
      * COMMUNES
      */
     
     private void chargeCommuneFromCSV() {
        BufferedReader br = null;
        String line;
        String cvsSplitBy = ";";

        
        try {
            String fichierSrc =  System.getProperty("user.dir") + File.separator + "fichiersSrc" + File.separator + "CommunesDépartementsRegions"+ File.separator + "comsimp2016-utf8.csv";
            br = new BufferedReader(new InputStreamReader(new FileInputStream(fichierSrc), "UTF-8"));
        
            //dep => 3
            //code => 4
            //nom => concatène 9 et 10
            
            this.lesCommunes = new HashMap();
            line = br.readLine();
            while ((line = br.readLine()) != null) {
                String[] comm = line.split(cvsSplitBy);
                String leLaL = comm[10];
                leLaL = leLaL.replace("(","");
                leLaL = leLaL.replace(")","");
                
                String nomComplet;
                if (leLaL.compareTo("Le") ==0||leLaL.compareTo("La")==0){
                    nomComplet = leLaL + " " + comm[11];
                }else{
                    //c'est un l' (ou rien) => pas d'espace
                    nomComplet = leLaL + comm[11];
                }
                
                String leLaLMaj = comm[8];
                leLaLMaj = leLaLMaj.replace("(","");
                leLaLMaj = leLaLMaj.replace(")","");
                
                String nomCompletMaj;
                if (leLaLMaj.compareTo("LE") ==0||leLaLMaj.compareTo("LA")==0){
                    nomCompletMaj = leLaLMaj + " " + comm[9];
                }else{
                    //c'est un l' => pas d'espace
                    nomCompletMaj = leLaLMaj + comm[9];
                }
                
                //s'il y a un 0 devant le code du département on l'enlève
                String dep = comm[3];
                /*if (dep.substring(0, 1).compareTo("0") == 0){
                    dep = comm[3].substring(1, comm[3].length());
                }*/
                
                //récupère le département à partir du code stocké dans la commune
                Departement d = this.lesDepartements.get(dep);
                Commune c = new Commune(comm[4], comm[11], nomCompletMaj, d);
                
                //la commune est une entité faible -> identifiant = concaténation de l'idf du département
                //avec l'identifiant de la commune
                String idfC = dep + comm[4];
                this.lesCommunes.put(idfC, c);
            }
            
        } catch (FileNotFoundException e) {
            Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException e) {
            Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }
        
        
        /**
         * Charge les codes postaux
         * 
         */
        
        br = null;

        try {
            String fichierSrc =  System.getProperty("user.dir") + File.separator + "fichiersSrc" + File.separator + "CommunesDépartementsRegions"+ File.separator + "laposte_hexasmal.csv";
            br = new BufferedReader(new InputStreamReader(new FileInputStream(fichierSrc), "UTF-8"));
           
            line = br.readLine();
            
            this.lesDepartements = new HashMap();
            
            br = new BufferedReader(new FileReader(fichierSrc));
            line = br.readLine();
            
            while ((line = br.readLine()) != null) {
                String[] com = line.split(cvsSplitBy);
                
                Commune c = this.lesCommunes.get(com[0]);
                if (c != null){
                    c.setCodePostal(com[2]);
                    c.setCoordonneesGPS(com[5]);
                }
                //else 
                    //on a pas trouvé le code de la commune du fichier de code postaux,
                    //dans notre liste de communes, on ne traite pas
                    //il y a plus de code postaux dans le fichier que dans les communes chargées
                    //(exemple pour les TOM, ils n'apparaissent pas dans les communes mais ils sont dans les CPostaux)
            }
            
        } catch (FileNotFoundException e) {
            Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException e) {
            Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }
    }    
    
    private void serialiseCommune() {
        BufferedReader br = null;
        String line;
        String cvsSplitBy = ";";

        try {
            String fichierDest =  System.getProperty("user.dir") + File.separator + "fichiersDest" + File.separator + "communes.ser";
            ObjectOutputStream oos =  new ObjectOutputStream(new FileOutputStream(fichierDest)) ;
            oos.writeObject(this.lesCommunes) ;
        } catch (FileNotFoundException e) {
            Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException e) {
            Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }
        
    }
    
    /**
     * PROFESSIONNELS DE SANTE
     */
    
    private void chargeProfSanteFromCSV() {
        BufferedReader br = null;
        String line;
        String cvsSplitBy = ";";

       try {
            String fichierSrc =  System.getProperty("user.dir") + File.separator + "fichiersSrc" + File.separator + "ProfsSante" + File.separator + "ExtractionMonoTable_CAT18_ToutePopulation_201703100734.csv";
            br = new BufferedReader(new InputStreamReader(new FileInputStream(fichierSrc), "UTF8"));
             
            this.lesProfessionelsDeSante = new HashMap();
            
            line = br.readLine();
            while ((line = br.readLine()) != null) {
                String[] medecin = line.split(cvsSplitBy);
                
                //public ProfessionnelSante(String identifiantPP, String identifiantNatPP, String nom, String prenom, String libelleCivilExercice, 
                //CategPro laCategorieProf, Structure laStructure, Profession laProfession) {
                
                CategPro c = this.lesCategPros.get(medecin[9].replace("\"", ""));
                Profession p = this.lesProfessions.get(medecin[7].replace("\"", ""));
                Structure s = this.lesStructures.get(medecin[21].replace("\"", ""));
                
                ProfessionnelSante ps = new ProfessionnelSante (medecin[1].replace("\"", ""), medecin[2].replace("\"", ""), 
                        medecin[5].replace("\"", ""), medecin[6].replace("\"", ""), medecin[4].replace("\"", ""), c, s, p);

                this.lesProfessionelsDeSante.put(medecin[1].replace("\"", ""), ps);
            }
            //out.close(); 
            //System.out.println(this.lesProfessionelsDeSante.get(10));
        } catch (FileNotFoundException e) {
            Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException e) {
            Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
        }finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }
    }
    
    private void serialiseProfSante() {
        BufferedReader br = null;
        String line;
        String cvsSplitBy = ";";

        try {
            String fichierDest =  System.getProperty("user.dir") + File.separator + "fichiersDest" + File.separator + "profsante.ser";
            ObjectOutputStream oos =  new ObjectOutputStream(new FileOutputStream(fichierDest)) ;
            oos.writeObject(this.lesProfessionelsDeSante) ;
        } catch (FileNotFoundException e) {
            Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException e) {
            Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }      
    }
 
    private void chargeProfSanteFIC() {
        BufferedReader br = null;
        String line;
      
        try {
            String fichierSrc =  System.getProperty("user.dir") + File.separator + "fichiersDest" + File.separator + "profsante.ser";
            FileInputStream fichier = new FileInputStream(fichierSrc);
            ObjectInputStream ois =  new ObjectInputStream(fichier) ;
            
            //deserialisation de l'arraylist
            this.lesProfessionelsDeSante = (HashMap<String, ProfessionnelSante>)ois.readObject();
            
        } catch (FileNotFoundException e) {
            Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException | ClassNotFoundException e) {
            Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }
    }
 
    
    /**
     * 
     * SITES
     * 
     */
    private void chargeSite() {
        BufferedReader br = null;
        String line;
        String cvsSplitBy = ";";
        this.lesSites = new HashMap();
        
        try {
            String fichierSrc =  System.getProperty("user.dir") + File.separator + "fichiersSrc" + File.separator + "ExtractionMonoTable_CAT18_ToutePopulation_201703100734.csv";
            br = new BufferedReader(new InputStreamReader(new FileInputStream(fichierSrc), "UTF8"));
            
            line = br.readLine();
            while ((line = br.readLine()) != null) {
                String[] site = line.split(cvsSplitBy);
                boolean trouve = this.rechercheSite(site[16]);
                int i =0;
                if (!trouve){
                    //public Site(String numSIRET, String numSIREN, String numFINESS, String numFINESSEtabJur, String raisonSociale, String enseigneComm) {
                    String prof = site[19].replace("\"", "");
                    prof = "\"" + prof + "\"";

                    this.lesSites.put(site[15].replace("\"", ""), new Site(site[15].replace("\"", ""), site[16].replace("\"", ""), site[17].replace("\"", ""), site[18].replace("\"", ""), site[19].replace("\"", ""), site[20].replace("\"", "")));
                    //ecris dans la destination
                } 
            }
            
        } catch (FileNotFoundException e) {
            Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException e) {
            Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
        }finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }
    }
  
    private boolean rechercheSite(String codeSite) {
        boolean trouve = false;
        int i = 0;
        while (trouve == false && i<this.lesSites.size()){
            if (this.lesSites.get(i).getNumSIREN().compareTo(codeSite) == 0){
                trouve = true;
            }        
            i++;
        }
        return trouve;
    }
    
    /**
     * 
     * STRUCTURES
     * 
     * 
     */
    private void chargeStructureFromCSV() {
        BufferedReader br = null;
        String line;
        String cvsSplitBy = ";";
        this.lesStructures = new HashMap();
        
        try {
            String fichierSrc =  System.getProperty("user.dir") + File.separator + "fichiersSrc" + File.separator + "ProfsSante" + File.separator + "ExtractionMonoTable_CAT18_ToutePopulation_201703100734.csv";
            br = new BufferedReader(new InputStreamReader(new FileInputStream(fichierSrc), "UTF8"));
            line = br.readLine();
            
            int i = 0;
            while ((line = br.readLine()) != null) {
                String[] profession = line.split(cvsSplitBy);
                
                if (profession[21].length() != 2){
                    //si la structure existe déjà, on ne l'insère pas
                    Structure s = this.lesStructures.get(profession[21]);
                    
                    if (s == null) {
                        //public Structure(String identifiantStruct, String cptDest, String cptPointGeo, 
                        //String numVoie, String indRepVoie, String codeTypeVoie, String libelTypeVoie, 
                        //String libelVoie, String mentionDist, String burCedex, String tel1, 
                        //String tel2, String telecopie, String mail, String siretSite) {
                        //this.identifiantStruct = identifiantStruct;
                        String  codeComm = profession[32].replace("\"", "");
                        Commune c = this.lesCommunes.get(codeComm);

                        this.lesStructures.put(profession[21].replace("\"", ""), new Structure(profession[21].replace("\"", ""), profession[22].replace("\"", ""), profession[23].replace("\"", ""), profession[24].replace("\"", ""), profession[25].replace("\"", ""), profession[26].replace("\"", ""), profession[27].replace("\"", "")
                                , profession[28].replace("\"", ""), profession[29].replace("\"", ""), profession[30].replace("\"", ""), profession[36].replace("\"", ""), profession[37].replace("\"", ""), profession[38].replace("\"", ""), profession[39].replace("\"", ""), profession[15].replace("\"", ""), c));
                    
                        //System.out.println(this.lesStructures.get(profession[21].replace("\"", "")));
                    }
                    
                }
            }                        
        } catch (FileNotFoundException e) {
            Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException e) {
            Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
        }finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }
    }
    
    /*private void serialiseStructure() {
        BufferedReader br = null;
        String line;
        String cvsSplitBy = ";";

        try {
            String fichierDest =  System.getProperty("user.dir") + File.separator + "fichiersDest" + File.separator + "structures.ser";
            ObjectOutputStream oos =  new ObjectOutputStream(new FileOutputStream(fichierDest)) ;
            oos.writeObject(this.lesStructures) ;
        } catch (FileNotFoundException e) {
            Logger.getLogger(ModeleFichiers.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException e) {
            Logger.getLogger(ModeleFichiers.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    Logger.getLogger(ModeleFichiers.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }      
    }
    
     private void chargeStructuresFIC() {
        BufferedReader br = null;
        String line;
      
        try {
            String fichierSrc =  System.getProperty("user.dir") + File.separator + "fichiersDest" + File.separator + "structures.ser";
            FileInputStream fichier = new FileInputStream(fichierSrc);
            ObjectInputStream ois =  new ObjectInputStream(fichier) ;
            
            //deserialisation de l'arraylist
            this.lesStructures = (HashMap<String, Structure>)ois.readObject();
            
        } catch (FileNotFoundException e) {
            Logger.getLogger(ModeleFichiers.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException | ClassNotFoundException e) {
            Logger.getLogger(ModeleFichiers.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    Logger.getLogger(ModeleFichiers.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }
    }
    
    */
    
    private boolean rechercheStructure(String struc, ArrayList<String> profession) {
        boolean trouve = false;
        int i = 0;
        while (trouve == false && i<profession.size()){
            if (profession.get(i).compareTo(struc) == 0){
                trouve = true;
            }        
            i++;
        }
        return trouve;
    }
    
    private boolean rechercheStructure(String codeStruct) {
        boolean trouve = false;
        int i = 0;
        while (trouve == false && i<this.lesStructures.size()){
            if (this.lesStructures.get(i).getIdentifiantStruct().compareTo(codeStruct) == 0){
                trouve = true;
            }        
            i++;
        }
        return trouve;
    }
 
   
    /**
     * 
     *  PROFESSION ET CATEG PROF
     * 
     */
            
    private void chargeProfessionEtCategProfFromCSV() {
        BufferedReader br = null;
        String line;
        String cvsSplitBy = ";";
        this.lesProfessions = new HashMap();
        this.lesCategPros = new HashMap();

        try {
            String fichierSrc = System.getProperty("user.dir") + File.separator + "fichiersSrc" + File.separator + "ProfsSante" + File.separator +  "ExtractionMonoTable_CAT18_ToutePopulation_201703100734.csv";            
            br = new BufferedReader(new InputStreamReader(new FileInputStream(fichierSrc), "UTF8"));
                     
            line = br.readLine();
            while ((line = br.readLine()) != null) {
                String[] profession = line.split(cvsSplitBy);
                Profession p = this.lesProfessions.get(profession[7].replace("\"", ""));
                
                if (p == null){
                    this.lesProfessions.put(profession[7].replace("\"", ""), new Profession(profession[7].replace("\"", ""), profession[8].replace("\"", ""))); 
                }
                
                CategPro cp = this.lesCategPros.get(profession[9].replace("\"", ""));
                
                if (cp == null){
                    this.lesCategPros.put(profession[9].replace("\"", ""), new CategPro( profession[9].replace("\"", ""), profession[10].replace("\"", "")));
                }
                
            }
            
        } catch (FileNotFoundException e) {
            Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException e) {
            Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
        }finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    Logger.getLogger(AccesDonnesProfSante.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }
    }
    
    /*private Profession rechercheProf(String codeProf) {
        Profession trouve = null;
        int i = 0;
        while (trouve == null && i<this.lesProfessions.size()){
            if (this.lesProfessions.get(i).getCode().compareTo(codeProf) == 0){
                trouve = this.lesProfessions.get(i);
            }        
            i++;
        }
        return trouve;
    }
    
    private CategPro rechercheCategPro(String codeProf) {
        CategPro trouve = null;
        int i = 0;
        while (trouve == null && i<this.lesCategPros.size()){
            if (this.lesCategPros.get(i).getCode().compareTo(codeProf) == 0){
                trouve = this.lesCategPros.get(i);
            }        
            i++;
        }
        return trouve;
    }*/
               
    /*private void serialiseProfessionEtCategProf() {
        BufferedReader br = null;
        String line;
        String cvsSplitBy = ";";

        try {
            String fichierDestPro =  System.getProperty("user.dir") + File.separator + "fichiersDest" + File.separator + "professions.ser";
            String fichierDestCategPro =  System.getProperty("user.dir") + File.separator + "fichiersDest" + File.separator + "categprof.ser";
            ObjectOutputStream oos =  new ObjectOutputStream(new FileOutputStream(fichierDestPro)) ;
            oos.writeObject(this.lesProfessions) ;
            ObjectOutputStream oos2 =  new ObjectOutputStream(new FileOutputStream(fichierDestCategPro)) ;
            oos2.writeObject(this.lesCategPros) ;
            
        } catch (FileNotFoundException e) {
            Logger.getLogger(ModeleFichiers.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException e) {
            Logger.getLogger(ModeleFichiers.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    Logger.getLogger(ModeleFichiers.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }      
    }
    
    private void chargeProfEtCategProFIC() {
        BufferedReader br = null;
        String line;
      
        try {
            String fichierSrcP =  System.getProperty("user.dir") + File.separator + "fichiersDest" + File.separator + "professions.ser";
            String fichierSrcCP =  System.getProperty("user.dir") + File.separator + "fichiersDest" + File.separator + "categprof.ser";
            FileInputStream fichier = new FileInputStream(fichierSrcP);
            FileInputStream fichier2 = new FileInputStream(fichierSrcCP);
            ObjectInputStream ois =  new ObjectInputStream(fichier) ;
            ObjectInputStream ois2 =  new ObjectInputStream(fichier2) ;

            //deserialisation de l'arraylist
            this.lesProfessions = (HashMap<String, Profession>)ois.readObject();
            this.lesCategPros = (HashMap<String, CategPro>)ois2.readObject();

        } catch (FileNotFoundException e) {
            Logger.getLogger(ModeleFichiers.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException | ClassNotFoundException e) {
            Logger.getLogger(ModeleFichiers.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    Logger.getLogger(ModeleFichiers.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }
    }*/
   
    
    
   
    
    /**
     * 
     * ACCESSEURS
     * 
     */
    
    public ArrayList<Region> getLesRegions(){
        return new ArrayList<>(this.lesRegions.values());
    }

}