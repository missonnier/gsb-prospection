/***********************************************************************
 * Module:  Departement.java
 * Author:  Fabrice Missonnier
 * Purpose: classe Departement
 ***********************************************************************/

package modele.classesmetiers.profsante;
import java.io.Serializable;

public class Departement implements Serializable{
    private String code;
    private String nom;
    private Region laRegion;

    public Departement(String code, String nom) {
        this.code = code;
        this.nom = nom;
    }
    
    public Departement(String code, String nom, Region laRegion) {
        this.code = code;
        this.nom = nom;
        this.laRegion = laRegion;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Region getLaRegion() {
        return laRegion;
    }

    public void setLaRegion(Region laRegion) {
        this.laRegion = laRegion;
    }

    @Override
    public String toString() {
        return super.toString() + "Departement{" + "code=" + code + ", nom=" + nom + ", laRegion=" + laRegion.toString() + '}';
    }

}
