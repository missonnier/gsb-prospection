/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.classesmetiers.profsante;

/**
 *
 * @author Fabrice Missonnier
 */
public class Site {
    private String numSIRET;
    private String numSIREN;
    private String numFINESS;
    private String numFINESSEtabJur;  
    private String raisonSociale;
    private String enseigneComm ;

    public Site(String numSIRET, String numSIREN, String numFINESS, String numFINESSEtabJur, String raisonSociale, String enseigneComm) {
        this.numSIRET = numSIRET;
        this.numSIREN = numSIREN;
        this.numFINESS = numFINESS;
        this.numFINESSEtabJur = numFINESSEtabJur;
        this.raisonSociale = raisonSociale;
        this.enseigneComm = enseigneComm;
    }

    public String getNumSIRET() {
        return numSIRET;
    }

    public void setNumSIRET(String numSIRET) {
        this.numSIRET = numSIRET;
    }

    public String getNumSIREN() {
        return numSIREN;
    }

    public void setNumSIREN(String numSIREN) {
        this.numSIREN = numSIREN;
    }

    public String getNumFINESS() {
        return numFINESS;
    }

    public void setNumFINESS(String numFINESS) {
        this.numFINESS = numFINESS;
    }

    public String getNumFINESSEtabJur() {
        return numFINESSEtabJur;
    }

    public void setNumFINESSEtabJur(String numFINESSEtabJur) {
        this.numFINESSEtabJur = numFINESSEtabJur;
    }

    public String getRaisonSociale() {
        return raisonSociale;
    }

    public void setRaisonSociale(String raisonSociale) {
        this.raisonSociale = raisonSociale;
    }

    public String getEnseigneComm() {
        return enseigneComm;
    }

    public void setEnseigneComm(String enseigneComm) {
        this.enseigneComm = enseigneComm;
    }

    @Override
    public String toString() {
        return "Site{" + "numSIRET=" + numSIRET + ", numSIREN=" + numSIREN + ", numFINESS=" + numFINESS + ", numFINESSEtabJur=" + numFINESSEtabJur + ", raisonSociale=" + raisonSociale + ", enseigneComm=" + enseigneComm + '}';
    }
}
