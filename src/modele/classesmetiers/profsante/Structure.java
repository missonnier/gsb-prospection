/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.classesmetiers.profsante;

import java.io.Serializable;

/**
 *
 * @author adgsb
 */
public class Structure implements Serializable{
    private String identifiantStruct;
    private String cptDest ;
    private String cptPointGeo ;
    private String numVoie ;  
    private String indRepVoie ;
    private String codeTypeVoie ;
    private String libelTypeVoie ;
    private String libelVoie ;
    private String mentionDist ;
    private String burCedex ;
    /*private String codePostal ;
    private String codeComm ;
    private String libelComm ;
    private String codePays ;
    private String libelPays ;*/
    private String tel1 ;
    private String tel2 ;
    private String telecopie ;
    private String mail ;
    private String numSiretSite;
    private Commune laCommune;
    
    
    /*public Structure(String identifiantStruct, String cptDest, String cptPointGeo, String numVoie, String indRepVoie, String codeTypeVoie, String libelTypeVoie, String libelVoie, String mentionDist, String burCedex, String codePostal, String codeComm, String libelComm, String codePays, String libelPays, String tel1, String tel2, String telecopie, String mail, String siretSite) {
        this.identifiantStruct = identifiantStruct;
        this.cptDest = cptDest;
        this.cptPointGeo = cptPointGeo;
        this.numVoie = numVoie;
        this.indRepVoie = indRepVoie;
        this.codeTypeVoie = codeTypeVoie;
        this.libelTypeVoie = libelTypeVoie;
        this.libelVoie = libelVoie;
        this.mentionDist = mentionDist;
        this.burCedex = burCedex;
        this.codePostal = codePostal;
        this.codeComm = codeComm;
        this.libelComm = libelComm;
        this.codePays = codePays;
        this.libelPays = libelPays;
        this.tel1 = tel1;
        this.tel2 = tel2;
        this.telecopie = telecopie;
        this.mail = mail;
        this.numSiretSite = siretSite;
    }*/

    public Structure(String identifiantStruct, String cptDest, String cptPointGeo, 
            String numVoie, String indRepVoie, String codeTypeVoie, String libelTypeVoie, 
            String libelVoie, String mentionDist, String burCedex, 
            String tel1, String tel2, String telecopie, String mail, String siretSite) {
        this.identifiantStruct = identifiantStruct;
        this.cptDest = cptDest;
        this.cptPointGeo = cptPointGeo;
        this.numVoie = numVoie;
        this.indRepVoie = indRepVoie;
        this.codeTypeVoie = codeTypeVoie;
        this.libelTypeVoie = libelTypeVoie;
        this.libelVoie = libelVoie;
        this.mentionDist = mentionDist;
        this.burCedex = burCedex;
        this.tel1 = tel1;
        this.tel2 = tel2;
        this.telecopie = telecopie;
        this.mail = mail;
        this.numSiretSite = siretSite;
    }
    
    public Structure(String identifiantStruct, String cptDest, String cptPointGeo, 
            String numVoie, String indRepVoie, String codeTypeVoie, String libelTypeVoie, 
            String libelVoie, String mentionDist, String burCedex,  
            String tel1, String tel2, String telecopie, String mail, String siretSite, Commune laCommune) {
        this.identifiantStruct = identifiantStruct;
        this.cptDest = cptDest;
        this.cptPointGeo = cptPointGeo;
        this.numVoie = numVoie;
        this.indRepVoie = indRepVoie;
        this.codeTypeVoie = codeTypeVoie;
        this.libelTypeVoie = libelTypeVoie;
        this.libelVoie = libelVoie;
        this.mentionDist = mentionDist;
        this.burCedex = burCedex;
        this.tel1 = tel1;
        this.tel2 = tel2;
        this.telecopie = telecopie;
        this.mail = mail;
        this.numSiretSite = siretSite;
        this.laCommune = laCommune;
    }

    
    public String getIdentifiantStruct() {
        return identifiantStruct;
    }

    public void setIdentifiantStruct(String identifiantStruct) {
        this.identifiantStruct = identifiantStruct;
    }

    public String getCptDest() {
        return cptDest;
    }

    public void setCptDest(String cptDest) {
        this.cptDest = cptDest;
    }

    public String getCptPointGeo() {
        return cptPointGeo;
    }

    public void setCptPointGeo(String cptPointGeo) {
        this.cptPointGeo = cptPointGeo;
    }

    public String getNumVoie() {
        return numVoie;
    }

    public void setNumVoie(String numVoie) {
        this.numVoie = numVoie;
    }

    public String getIndRepVoie() {
        return indRepVoie;
    }

    public void setIndRepVoie(String indRepVoie) {
        this.indRepVoie = indRepVoie;
    }

    public String getCodeTypeVoie() {
        return codeTypeVoie;
    }

    public void setCodeTypeVoie(String codeTypeVoie) {
        this.codeTypeVoie = codeTypeVoie;
    }

    public String getLibelTypeVoie() {
        return libelTypeVoie;
    }

    public void setLibelTypeVoie(String libelTypeVoie) {
        this.libelTypeVoie = libelTypeVoie;
    }

    public String getLibelVoie() {
        return libelVoie;
    }

    public void setLibelVoie(String libelVoie) {
        this.libelVoie = libelVoie;
    }

    public String getMentionDist() {
        return mentionDist;
    }

    public void setMentionDist(String mentionDist) {
        this.mentionDist = mentionDist;
    }

    public String getBurCedex() {
        return burCedex;
    }

    public void setBurCedex(String burCedex) {
        this.burCedex = burCedex;
    }

    /*public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getCodeComm() {
        return codeComm;
    }

    public void setCodeComm(String codeComm) {
        this.codeComm = codeComm;
    }

    public String getLibelComm() {
        return libelComm;
    }

    public void setLibelComm(String libelComm) {
        this.libelComm = libelComm;
    }

    public String getCodePays() {
        return codePays;
    }

    public void setCodePays(String codePays) {
        this.codePays = codePays;
    }

    public String getLibelPays() {
        return libelPays;
    }

    public void setLibelPays(String libelPays) {
        this.libelPays = libelPays;
    }*/

    public String getTel1() {
        return tel1;
    }

    public void setTel1(String tel1) {
        this.tel1 = tel1;
    }

    public String getTel2() {
        return tel2;
    }

    public void setTel2(String tel2) {
        this.tel2 = tel2;
    }

    public String getTelecopie() {
        return telecopie;
    }

    public void setTelecopie(String telecopie) {
        this.telecopie = telecopie;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getNumSiretSite() {
        return numSiretSite;
    }

    public void setNumSiretSite(String numSiretSite) {
        this.numSiretSite = numSiretSite;
    }

    public Commune getLaCommune() {
        return laCommune;
    }

    public void setLaCommune(Commune laCommune) {
        this.laCommune = laCommune;
    }
    
    @Override
    public boolean equals(Object obj) {
         if(obj instanceof Structure)
        {
            Structure temp = (Structure) obj;
            if(this.identifiantStruct.compareTo(temp.identifiantStruct) ==0)
                return true;
        }
        return false;

    }
    @Override
    public int hashCode() {
        return (this.identifiantStruct.hashCode());        
    }

    @Override
    public String toString() {
        return "Structure{" + "identifiantStruct=" + identifiantStruct + ", cptDest=" + cptDest + ", cptPointGeo=" + cptPointGeo + ", numVoie=" + numVoie + ", indRepVoie=" + indRepVoie + ", codeTypeVoie=" + codeTypeVoie + ", libelTypeVoie=" + libelTypeVoie + ", libelVoie=" + libelVoie + ", mentionDist=" + mentionDist + ", burCedex=" + burCedex + ", tel1=" + tel1 + ", tel2=" + tel2 + ", telecopie=" + telecopie + ", mail=" + mail + ", numSiretSite=" + numSiretSite + ", laCommune=" + laCommune + '}';
    }
    
    
    
}
