/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.classesmetiers.profsante;

import java.io.Serializable;

/**
 *
 * @author Fabrice Missonnier
 */
public class Profession implements Serializable{
    private String code;
    private String nom;

    public Profession(String code, String nom) {
        this.code = code;
        this.nom = nom;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Profession{" + "code=" + code + ", nom=" + nom + '}';
    }
    
}
