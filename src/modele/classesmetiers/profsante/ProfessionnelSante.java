/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.classesmetiers.profsante;
import java.io.Serializable;
import modele.labo.CategPro;


public class ProfessionnelSante implements Serializable{
    private String identifiantPP;
    private String identifiantNatPP;
    private String nom;
    private String prenom;
    private String libelleCivilExercice; //Docteur par exemple
    private CategPro laCategorieProf;
    private Structure laStructure;
    private Profession laProfession;

    public ProfessionnelSante(String identifiantPP, String identifiantNatPP, String nom, String prenom, String libelleCivilExercice) {
        this.identifiantPP = identifiantPP;
        this.identifiantNatPP = identifiantNatPP;
        this.nom = nom;
        this.prenom = prenom;
        this.libelleCivilExercice = libelleCivilExercice;
        this.laCategorieProf = null;
        this.laStructure = null;
        this.laProfession = null;
    }
    
    public ProfessionnelSante(String identifiantPP, String identifiantNatPP, String nom, String prenom, String libelleCivilExercice, CategPro laCategorieProf, Structure laStructure, Profession laProfession) {
        this.identifiantPP = identifiantPP;
        this.identifiantNatPP = identifiantNatPP;
        this.nom = nom;
        this.prenom = prenom;
        this.libelleCivilExercice = libelleCivilExercice;
        this.laCategorieProf = laCategorieProf;
        this.laStructure = laStructure;
        this.laProfession = laProfession;
    }
    
    public String getIdentifiantPP() {
        return identifiantPP;
    }

    public void setIdentifiantPP(String identifiantPP) {
        this.identifiantPP = identifiantPP;
    }

    public String getIdentifiantNatPP() {
        return identifiantNatPP;
    }

    public void setIdentifiantNatPP(String identifiantNatPP) {
        this.identifiantNatPP = identifiantNatPP;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public CategPro getLaCategorieProf() {
        return laCategorieProf;
    }

    public void setLaCategorieProf(CategPro laCategorieProf) {
        this.laCategorieProf = laCategorieProf;
    }

    public Structure getLaStructure() {
        return laStructure;
    }

    public void setLaStructure(Structure laStructure) {
        this.laStructure = laStructure;
    }

    public Profession getLaProfession() {
        return laProfession;
    }

    public void setLaProfession(Profession laProfession) {
        this.laProfession = laProfession;
    }

    public String getLibelleCivilExercice() {
        return libelleCivilExercice;
    }

    public void setLibelleCivilExercice(String libelleCivilExercice) {
        this.libelleCivilExercice = libelleCivilExercice;
    }

    @Override
    public String toString() {
        return "ProfessionelSante{" + "identifiantPP=" + identifiantPP + ", identifiantNatPP=" + identifiantNatPP + ", nom=" + nom + ", prenom=" + prenom + ", libelleCivilExercice=" + libelleCivilExercice + ", "
                + "\n\t\tlaCategorieProf=" + laCategorieProf + "\n\t\tlaStructure=" + laStructure + "\n\t\tlaProfession=" + laProfession + '}';
    }
    
}
