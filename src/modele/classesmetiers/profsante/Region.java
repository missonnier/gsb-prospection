/***********************************************************************
 * Module:  Region.java
 * Author:  Fabrice Missonnier
 * Purpose: classe Region
 ***********************************************************************/

package modele.classesmetiers.profsante;

import java.io.Serializable;

public class Region implements Serializable {
    private String code;
    private String nom;

    public Region(String code, String nom) {
        this.code = code;
        this.nom = nom;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return super.toString() + "Region{" + "code=" + code + ", nom=" + nom + '}';
    }
}
