/***********************************************************************
 * Module:  Commune.java
 * Author:  Fabrice Missonnier
 * Purpose: classe Commune
 ***********************************************************************/

package modele.classesmetiers.profsante;
import java.io.Serializable;

public class Commune implements Serializable{
    private String code;
    private String nom;
    private String nomMaj;
    private String codePostal;
    private String coordonneesGPS;
    private Departement leDepartement;
 
    public Commune(String code, String nom, String nomMaj) {
      this.code = code;
      this.nom = nom;
      this.nomMaj = nomMaj;
    }

    public Commune(String code, String nom, String nomMaj, Departement leDep) {
        this.code = code;
        this.nom = nom;
        this.nomMaj = nomMaj;
        this.leDepartement = leDep;
    }
    
    public Departement getLeDepartement() {
        return leDepartement;
    }

    public void setLeDepartement(Departement leDep) {
        this.leDepartement = leDep;
    }

    public String getNomMaj() {
        return nomMaj;
    }

    public void setNomMaj(String nomMaj) {
        this.nomMaj = nomMaj;
    }
 
    public String getCode() {
       return code;
    }

    public void setCode(String code) {
       this.code = code;
    }

    public String getNom() {
       return nom;
    }

    public void setNom(String nom) {
       this.nom = nom;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getCoordonneesGPS() {
        return coordonneesGPS;
    }

    public void setCoordonneesGPS(String coordonneesGPS) {
        this.coordonneesGPS = coordonneesGPS;
    }

    @Override
    public String toString() {
        return "Commune{" + "code=" + code + ", nom=" + nom + ", nomMaj=" + nomMaj + ", codePostal=" + codePostal + ", coordonneesGPS=" + coordonneesGPS + ", leDepartement=" + leDepartement + '}';
    }
}