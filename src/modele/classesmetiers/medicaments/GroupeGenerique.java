/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.classesmetiers.medicaments;

/**
 *
 * @author fabrice_2
 */
public class GroupeGenerique {
    /*
    Identifiant du groupe générique 
-  Libellé du groupe générique 
-  Code CIS 
-  Type de générique, avec les valeurs suivantes : 
o  0 : « princeps » 
o  1 : « générique » 
o  2 : « génériques par complémentarité posologique » 
o  4 : « générique substituable » 
-  Numéro permettant de trier les éléments d’un groupe
    */
    private String identifiant;
    private String libelle;
    private TypeGenerique leType;

    public GroupeGenerique(String identifiant, String libelle, TypeGenerique leType) {
        this.identifiant = identifiant;
        this.libelle = libelle;
        this.leType = leType;
    }

    public String getIdentifiant() {
        return identifiant;
    }

    public void setIdentifiant(String identifiant) {
        this.identifiant = identifiant;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public TypeGenerique getLeType() {
        return leType;
    }

    public void setLeType(TypeGenerique leType) {
        this.leType = leType;
    }

    @Override
    public String toString() {
        return "GroupeGenerique{" + "identifiant=" + identifiant + ", libelle=" + libelle + ", \n\t\tleType=" + leType + '}';
    }
    
    
}
