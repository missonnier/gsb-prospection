/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.classesmetiers.medicaments;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 *
 * @author Fabrice Missonnier
 */
public class Medicament {
    /*Code CIS 
    Dénomination du médicament 
    Forme pharmaceutique 
    Voies d'administration 
    Statut administratif de l’AMM 
    Type de procédure d'AMM 
    Etat de commercialisation 
    Date d’AMM (format JJ/MM/AAAA) 
    StatutBdm : «Alerte»/ «disponibilité » 
    Numéro de l’autorisation européenne 
    Titulaire(s) 
    Surveillance renforcée (triangle noir) Oui/Non
    
    Code CIS                            [0]<66659617> 
    Dénomination du médicament          [1]<ESBERIVEN FORT, solution buvable en ampoule> 
    Forme pharmaceutique                [2]<solution buvable> 
    Voies d'administration              [3]<orale> 
    Statut administratif de l AMM       [4]<Autorisation abrogée> 
    Type de procédure d'AMM             [5]<Procédure nationale> 
    Etat de commercialisation           [6]<Non commercialisée> 
    Date d’AMM (format JJ/MM/AAAA)      [7]<04/03/1992> 
    StatutBdm : «Alerte»/ «disponibilité » [8]<Warning disponibilité> 
    Numéro de l'autorisation européenne [9]<> 
    Titulaire(s)                        [10]< AMDIPHARM> 
    Surveillance renforcée (triangle noir) Oui/Non  [11]<Non> 
    */
    
    private String codeCIS;
    private String denomination;
    private String formePharma;
    private String voieAdmin;
    private String statutAdminAMM;
    private String typeProcAMM;
    private String etatCommercialisation;
    private Date dateAMM;
    private String statusBDM;
    private String numAutEurop;
    private String titulaire;
    private boolean surveillance;
    private String conditionPrescription;
    
    private ArrayList<Presentation> lesPresentations;
    private HashMap<Substance, Dosage> composition;
    private ArrayList<AvisHAS> lesAvis;
    private ArrayList<GroupeGenerique> lesGeneriques;

    public Medicament(String codeCIS, String denomination, String formePharma, String voieAdmin, String statutAdminAMM, String typeProcAMM, String etatCommercialisation, Date dateAMM, String statusBDM, String numAutEurop, String titulaire, boolean surveillance) {
        this.codeCIS = codeCIS;
        this.denomination = denomination;
        this.formePharma = formePharma;
        this.voieAdmin = voieAdmin;
        this.statutAdminAMM = statutAdminAMM;
        this.typeProcAMM = typeProcAMM;
        this.etatCommercialisation = etatCommercialisation;
        this.dateAMM = dateAMM;
        this.statusBDM = statusBDM;
        this.numAutEurop = numAutEurop;
        this.titulaire = titulaire;
        this.surveillance = surveillance;
        this.conditionPrescription = "";
        this.lesPresentations = new ArrayList();
        this.composition = new HashMap();
        this.lesAvis = new ArrayList();
        this.lesGeneriques = new ArrayList();
    }

    public String getCodeCIS() {
        return codeCIS;
    }

    public void setCodeCIS(String codeCIS) {
        this.codeCIS = codeCIS;
    }

    public String getDenomination() {
        return denomination;
    }

    public void setDenomination(String denomination) {
        this.denomination = denomination;
    }

    public String getFormePharma() {
        return formePharma;
    }

    public void setFormePharma(String formePharma) {
        this.formePharma = formePharma;
    }

    public String getVoieAdmin() {
        return voieAdmin;
    }

    public void setVoieAdmin(String voieAdmin) {
        this.voieAdmin = voieAdmin;
    }

    public String getStatutAdminAMM() {
        return statutAdminAMM;
    }

    public void setStatutAdminAMM(String statutAdminAMM) {
        this.statutAdminAMM = statutAdminAMM;
    }

    public String getTypeProcAMM() {
        return typeProcAMM;
    }

    public void setTypeProcAMM(String typeProcAMM) {
        this.typeProcAMM = typeProcAMM;
    }

    public String getEtatCommercialisation() {
        return etatCommercialisation;
    }

    public void setEtatCommercialisation(String etatCommercialisation) {
        this.etatCommercialisation = etatCommercialisation;
    }

    public Date getDateAMM() {
        return dateAMM;
    }

    public void setDateAMM(Date dateAMM) {
        this.dateAMM = dateAMM;
    }

    public String getStatusBDM() {
        return statusBDM;
    }

    public void setStatusBDM(String statusBDM) {
        this.statusBDM = statusBDM;
    }

    public String getNumAutEurop() {
        return numAutEurop;
    }

    public void setNumAutEurop(String numAutEurop) {
        this.numAutEurop = numAutEurop;
    }

    public String getTitulaire() {
        return titulaire;
    }

    public void setTitulaire(String titulaire) {
        this.titulaire = titulaire;
    }

    public boolean isSurveillance() {
        return surveillance;
    }

    public void setSurveillance(boolean surveillance) {
        this.surveillance = surveillance;
    }

    public ArrayList<Presentation> getLesPresentations() {
        return this.lesPresentations;
    }

    public void setPresentation(Presentation laPresentation) {
        this.lesPresentations.add(laPresentation);
    }

    public void setComposant(Substance laSubstance, Dosage leDosage) {
        this.composition.put(laSubstance, leDosage);
    }

    public void setAvis(AvisHAS lavis) {
        this.lesAvis.add(lavis);
    }

    public void setLeGenerique(GroupeGenerique leGenerique) {
        this.lesGeneriques.add(leGenerique) ;
    }

    public String getConditionPrescription() {
        return conditionPrescription;
    }

    public void setConditionPrescription(String conditionPrescription) {
        this.conditionPrescription = conditionPrescription;
    }

    public HashMap<Substance, Dosage> getComposition() {
        return composition;
    }

    public ArrayList<AvisHAS> getLesAvis() {
        return lesAvis;
    }

    public ArrayList<GroupeGenerique> getLesGeneriques() {
        return lesGeneriques;
    }

    @Override
    public String toString() {
        String s = "Medicament{" + "codeCIS=" + codeCIS + ", denomination=" + denomination + ", formePharma=" + formePharma + ", voieAdmin=" + voieAdmin + ", statutAdminAMM=" + statutAdminAMM + ", typeProcAMM=" + typeProcAMM + ", etatCommercialisation=" + etatCommercialisation + ", dateAMM=" + dateAMM + ", statusBDM=" + statusBDM + ", numAutEurop=" + numAutEurop + ", titulaire=" + titulaire + ", surveillance=" + surveillance + ", conditionPrescription=" + conditionPrescription + ", lesPresentations=";
        
        for (Presentation p : this.lesPresentations){
                s = s + "\n\t\t" + p.toString();
        }
        
        s = s + "\n, composition=" + composition + ", \n\tlesAvis=" + lesAvis + ", \n\tlesGeneriques=" + lesGeneriques + '}';
        return s;
    }
    
}
