/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.classesmetiers.medicaments;

import java.util.Date;

/**
 *
 * @author fabrice_2
 */
public abstract class AvisHAS {
    /*Code CIS 
-  Code de dossier HAS 
-  Motif d’évaluation 
-  Date de l’avis de la Commission de la transparence (format AAAAMMJJ) 
    
 + lien vers l'avis
*/
    
    private String codeDossier;
    private String motifEval;
    private Date dateAvis;
    
    private LienAvis leLien;
 
    public AvisHAS(String codeDossier, String motifEval, Date dateAvis) {
        this.codeDossier = codeDossier;
        this.motifEval = motifEval;
        this.dateAvis = dateAvis;
    }

    public AvisHAS(String codeDossier, String motifEval, Date dateAvis, LienAvis leLien) {
        this.codeDossier = codeDossier;
        this.motifEval = motifEval;
        this.dateAvis = dateAvis;
        this.leLien = leLien;
    }
    
    public String getCodeDossier() {
        return codeDossier;
    }

    public void setCodeDossier(String codeDossier) {
        this.codeDossier = codeDossier;
    }

    public String getMotifEval() {
        return motifEval;
    }

    public void setMotifEval(String motifEval) {
        this.motifEval = motifEval;
    }

    public Date getDateAvis() {
        return dateAvis;
    }

    public void setDateAvis(Date dateAvis) {
        this.dateAvis = dateAvis;
    }

    public LienAvis getLeLien() {
        return leLien;
    }

    public void setLeLien(LienAvis leLien) {
        this.leLien = leLien;
    }

    @Override
    public String toString() {
        return "AvisHAS{" + "codeDossier=" + codeDossier + ", motifEval=" + motifEval + ", dateAvis=" + dateAvis + ", leLien=" + leLien + '}';
    }
    
}
