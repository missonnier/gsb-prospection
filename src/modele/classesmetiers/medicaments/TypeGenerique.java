/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.classesmetiers.medicaments;

/**
 *
 * @author fabrice_2
 */
public class TypeGenerique {
    private String codeTG;
    private String libelleTG;

    public TypeGenerique(String codeTG, String libelleTG) {
        this.codeTG = codeTG;
        this.libelleTG = libelleTG;
    }

    public String getCodeTG() {
        return codeTG;
    }

    public void setCodeTG(String codeTG) {
        this.codeTG = codeTG;
    }

    public String getLibelleTG() {
        return libelleTG;
    }

    public void setLibelleTG(String libelleTG) {
        this.libelleTG = libelleTG;
    }

    @Override
    public String toString() {
        return "TypeGenerique{" + "codeTG=" + codeTG + ", libelleTG=" + libelleTG + '}';
    }
    
}
