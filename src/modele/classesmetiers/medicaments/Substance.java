/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.classesmetiers.medicaments;

/**
 *
 * @author fabrice_2
 */
public class Substance {
    /*Code CIS => pas ici (c'est dans le médicament)
-  Code de la substance 
-  Dénomination de la substance 
 */
    
    private String code;
    private String denomination;

    public Substance(String code, String denomination) {
        this.code = code;
        this.denomination = denomination;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDenomination() {
        return denomination;
    }

    public void setDenomination(String denomination) {
        this.denomination = denomination;
    }

    @Override
    public String toString() {
        return "Substance{" + "code=" + code + ", denomination=" + denomination + '}';
    }
    
}
