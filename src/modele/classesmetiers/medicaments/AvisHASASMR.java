/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.classesmetiers.medicaments;

import java.util.Date;

/**
 *
 * @author fabrice_2
 */
public class AvisHASASMR extends AvisHAS {
    /*
-  Valeur du ASMR 
-  Libellé du ASMR */

    private String valeurASMR;
    private String libelleASMR;

    public AvisHASASMR(String codeDossier, String motifEval, Date dateAvis, String valeurASMR, String libelleASMR) {
        super (codeDossier, motifEval, dateAvis);
        this.valeurASMR = valeurASMR;
        this.libelleASMR = libelleASMR;
    }
    
    public AvisHASASMR(String codeDossier, String motifEval, Date dateAvis, LienAvis leLien, String valeurASMR, String libelleASMR) {
        super (codeDossier, motifEval, dateAvis, leLien);
        this.valeurASMR = valeurASMR;
        this.libelleASMR = libelleASMR;
    }
    
    public String getValeurASMR() {
        return valeurASMR;
    }

    public void setValeurASMR(String valeurASMR) {
        this.valeurASMR = valeurASMR;
    }

    public String getLibelleASMR() {
        return libelleASMR;
    }

    public void setLibelleASMR(String libelleASMR) {
        this.libelleASMR = libelleASMR;
    }

    @Override
    public String toString() {
        return "AvisHASASMR{" + super.toString() + " valeurASMR=" + valeurASMR + ", libelleASMR=" + libelleASMR + '}';
    }
}
