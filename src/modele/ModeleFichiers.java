/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

import java.util.ArrayList;
import modele.classesmetiers.profsante.Region;
import modele.classesmetiers.medicaments.Medicament;




/**
 * Created by Fabrice on 01/10/2017.
 */

public class ModeleFichiers{
    private  AccesDonnesMedicaments m;
    private AccesDonnesProfSante pf;
    
    public ModeleFichiers() {
        m = new AccesDonnesMedicaments();
        pf = new AccesDonnesProfSante();
    }
    
    public ArrayList<Medicament> getLesMedicaments(){
        return m.getLesMedicaments();
    }
    
    public ArrayList<Region> getLesRegions(){
        return pf.getLesRegions();
    }
}