/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.labo;

import java.util.Date;

/**
 *
 * @author eleve
 */
public class Visiteur {

    private String matricule;
    private String nom;
    private String prenom;
    private String adresse;
    private String codePostal;
    private String ville;
    private Date dateEmbauche;
    private String codeSecteur;
    private String codeLaboratoire;

    public Visiteur(String matricule, String nom, String prenom, String adresse, String codePostal,
             String ville, Date dateEmbauche,  String codeSecteur, String codeLaboratoire) {
        this.matricule = matricule;
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.ville = ville;
        this.codeSecteur = codeSecteur;
        this.codeLaboratoire = codeLaboratoire;
        this.codePostal = codePostal;
        this.dateEmbauche = dateEmbauche;
    }

    public void setMatricule(String matricule) {
        this.matricule = matricule;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public void setCodeSecteur(String codeSecteur) {
        this.codeSecteur = codeSecteur;
    }

    public void setCodeLaboratoire(String codeLaboratoire) {
        this.codeLaboratoire = codeLaboratoire;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public void setDateEmbauche(Date dateEmbauche) {
        this.dateEmbauche = dateEmbauche;
    }

    public String getMatricule() {
        return matricule;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getAdresse() {
        return adresse;
    }

    public String getVille() {
        return ville;
    }

    public String getCodeSecteur() {
        return codeSecteur;
    }

    public String getCodeLaboratoire() {
        return codeLaboratoire;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public Date getDateEmbauche() {
        return dateEmbauche;
    }
}
