/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.labo;

import java.io.Serializable;

/**
 *
 * @author adgsb
 */
public class CategPro implements Serializable{
    private String code;
    private String nom;

    public CategPro(String code, String nom) {
        this.code = code;
        this.nom = nom;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "CategPro{" + "code=" + code + ", nom=" + nom + '}';
    }
   
}
