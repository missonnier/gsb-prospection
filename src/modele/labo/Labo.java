package modele.labo;

/**
 *
 * @author BESSE Pauline
 */
public class Labo {
    private String codeL;
    private String nomL;    
    private String chefVenteLabo;
   
    public Labo() {
        this.codeL = "";
        this.nomL = "";
        this.chefVenteLabo = "";
       
    }
    
     public Labo(String nomL) {
        this.nomL = nomL;  
    }
    public Labo(String nomL, String chefVenteLabo) {
        this.nomL = nomL;
        this.chefVenteLabo = chefVenteLabo;
        
    }

    public Labo(String codeL, String nomL, String chefVenteLabo) {
        this.codeL = codeL;
        this.nomL = nomL;
        this.chefVenteLabo = chefVenteLabo;
        
    }

    public String getCodeL() {
        return codeL;
    }

    public void setcodeL(String codeL) {
        this.codeL= codeL;
    }

    public String getNomL() {
        return nomL;
    }

    public void setNomL(String nomL) {
        this.nomL = nomL;
    }

    public String getChefVenteLabo() {
        return chefVenteLabo;
    }

    public void setChefVenteLabo(String chefVenteLabo) {
        this.chefVenteLabo = chefVenteLabo;
    }

    
    
    

}
