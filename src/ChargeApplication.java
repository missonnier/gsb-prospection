import modele.ModeleFichiers;
import controle.ControleurSelectionRegion;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import modele.ModeleFichiers;
import vue.FenetreSelectionRegion;

/**
 *
 * @author Fabrice Missonnier
 */
public class ChargeApplication {
    
    public static void main(String[] args) {
        //chargeApplication();
        chargeApplication();
    }
    
    public static void chargeApplication() {
        /*
         ******************************************************
         * Choix du modèle en fonction ce ce qu'il y a dans
         * le fichier properties
         ******************************************************
         */
          

        ModeleFichiers monModele = null;
        String fichierProp = "." + System.getProperty("file.separator") + "build" + System.getProperty("file.separator") 
                + "classes" + System.getProperty("file.separator") + "proprietesappli.properties";
        
        Properties properties = new Properties();
        FileInputStream input ;
       
        try {
            input = new FileInputStream(fichierProp);
            //on regarde sur quel modèle on pointe dans le fichier properties
            properties.load(input);
                   
            if (properties.getProperty("modele").compareTo("SQLServerProcStockees")==0){
                //on récupère l'adresse IP et le nom de la BDD dans le fichier properties
                String adIP = properties.getProperty("adresseServeurSQLServer");
                String nomBase = properties.getProperty("nomBDDSQLServer"); 
                String idf = properties.getProperty("idfSQLServer");
                String mdp = properties.getProperty("mdpSQLServer"); 
                
                //création du modèle
                //monModele = new ModeleSQLServerProceduresStockees(adIP, nomBase, idf, mdp);
            }
            else if (properties.getProperty("modele").compareTo("SQLServer")==0){
                //on récupère l'adresse IP et le nom de la BDD dans le fichier properties
                String adIP = properties.getProperty("adresseServeurSQLServer");
                String nomBase = properties.getProperty("nomBDDSQLServer"); 
                String idf = properties.getProperty("idfSQLServer");
                String mdp = properties.getProperty("mdpSQLServer"); 
                
                //création du modèle
                //monModele = new ModeleSQLServer(adIP, nomBase, idf, mdp);
            }
            else if (properties.getProperty("modele").compareTo("Hibernate")==0){
                //monModele = new ModeleHibernate();
            }
            else if (properties.getProperty("modele").compareTo("Fichiers")==0){
                monModele = new ModeleFichiers();
            }
            else {
                System.out.println("Erreur lors du chargement de l'application, propriété inconnue");
            }
            input.close();
        } catch (IOException ex) {
            Logger.getLogger(ChargeApplication.class.getName()).log(Level.SEVERE, null, ex);
        }
        
         /*
         ******************************************************
         * 
         * Instanciation de la vue et du controleur
         * 
         ******************************************************
         */
        
        FenetreSelectionRegion fsr = new FenetreSelectionRegion();
        ControleurSelectionRegion csr = new ControleurSelectionRegion(fsr, monModele);
        fsr.setControleur(csr);
        fsr.setVisible(true);
        
    }
}
